package systextil.cavalheiro.pcpc.etiquetas;

import br.com.intersys.systextil.connection.AppConnection;
import br.com.intersys.systextil.exception.NoDataException;
import br.com.intersys.systextil.global.JasperReport;
import br.com.intersys.systextil.global.Mdi;
import br.com.intersys.systextil.global.TagException;
import java.util.ArrayList;

class EtiquetasPcpcE030 implements systextil.plugin.pcpc.etiquetas.PcpcE030 {

    private final Mdi mdi;
    private final ArrayList<Registro> registros = new ArrayList<EtiquetasPcpcE030.Registro>(100);
    private final AppConnection conn;

    public static class Registro {

        private String referencia, narrativa, codigoBarras, tamanho, linhaProd;

        public String getNarrativa() {
            return narrativa;
        }

        public String getReferencia() {
            return referencia;
        }

        public String getCodigoBarras() {
            return codigoBarras;
        }

        public String getTamanho() {
            return tamanho;
        }

        public String getLinhaProd() {
            return linhaProd;
        }
    }

    EtiquetasPcpcE030(AppConnection conn, Mdi mdi) {
        this.conn = conn;
        this.mdi = mdi;
    }

    @Override
    public void processarRegistro(Dto dto) throws TagException {
        Registro reg = new Registro();
        reg.narrativa = dto.narrativa;
        reg.referencia = dto.gru_prod;
        reg.codigoBarras = String.format("%04d%09d%05d%04d", dto.periodo_producao, dto.ordem_producao, dto.ordem_confeccao, dto.contador);
        reg.tamanho = dto.sub_prod;
        reg.linhaProd = "";

        int linha_prod = 0;
        String cmdSqlProd = "select linha_produto from basi_030 "
                + "where referencia = ?";
        AppConnection cnSeq = new AppConnection(conn, cmdSqlProd, dto.gru_prod);
        cnSeq.executeQuery();
        while (cnSeq.next()) {
            linha_prod = cnSeq.getInt("linha_produto");

            String cmdSqlCol = "select descricao_linha from basi_120 "
                    + "where linha_produto = ?";
            AppConnection cnSeq1 = new AppConnection(conn, cmdSqlCol, linha_prod);
            cnSeq1.executeQuery();
            while (cnSeq1.next()) {
                reg.linhaProd = cnSeq1.getString("descricao_linha");
            }
            cnSeq1.close();
        }
        cnSeq.close();

        registros.add(reg);
    }

    private String getLayoutName() {
        return "PcpcE030";
    }

    @Override
    public void imprimir() throws TagException, NoDataException {
        if (registros.isEmpty()) {
            throw new NoDataException();
        }
        JasperReport.imprimirEtiquetas(mdi.impressora_padrao_etiqueta, getClass().getResourceAsStream(getLayoutName() + ".jasper"),
                registros);
    }
}