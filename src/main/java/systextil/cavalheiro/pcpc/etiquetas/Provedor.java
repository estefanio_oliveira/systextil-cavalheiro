package systextil.cavalheiro.pcpc.etiquetas;

import br.com.intersys.systextil.connection.AppConnection;
import br.com.intersys.systextil.global.Mdi;
import systextil.plugin.pcpc.etiquetas.PcpcE030;

public class Provedor implements systextil.plugin.pcpc.etiquetas.Provedor {

    @Override
    public PcpcE030 getPcpcE030(AppConnection conn, Mdi mdi) {
        return new EtiquetasPcpcE030(conn, mdi);
    }

    @Override
    public boolean usaTabelaDePrecos(String nome_form) {
        return false;
    }
    
}
